Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System

Public Module Main
		
	Public Sub Main()
		Try
			Dim args() As String = Environment.GetCommandLineArgs()
			
			Dim seed As Integer = If(args.Length > 1, CInt(args(1)), 1)
		
			Dim rand As New Random(seed)
			Dim N As Integer = rand.Next(100, 1001)
			Dim weight(N * N - 1) As Double			
			For i As Integer = 0 To UBound(weight)
				weight(i) = NextGaussian(rand)
			Next i
			For i As Integer = 0 To N - 1
				weight(i * N + i) = 0.0
			Next i
			
			Console.WriteLine("Seed = {0}, N = {1}", seed, N)
			
			Dim pr As New Permute()
			Dim w(N * N - 1) As Double
			Array.Copy(weight, w, w.Length)
			Dim ret() As Integer = pr.findOrder(w)
			
			If ret Is Nothing Then
				Console.Error.WriteLine("ret Is Nothing")
				Exit Sub
			End If
			
			If ret.Length <> N Then
				Console.Error.WriteLine("ret.Length is {0}, not N({1})", ret.Length, N)
				Exit Sub
			End If
			
			Dim chk(N - 1) As Integer
			Array.Copy(ret, chk, chk.Length)
			Array.Sort(chk)
			For i As Integer = 0 To N - 1
				If chk(i) <> i Then
					Console.Error.WriteLine("Not permutation")
					Exit Sub
				End If
			Next i

			Dim score As Double = 0.0
			For i As Integer = 0 To N - 2
				For j As Integer = i + 1 To N - 1
					score += weight(ret(i) * N + ret(j))
				Next j
			Next i
			Dim DN As Double = CDbl(N)
			score = score / (DN * Math.Sqrt(DN * Math.Log(DN) - DN) / 100.0)
			Console.WriteLine("Score = {0:F}", If(score < 0.0, 0.0, score))
			
		Catch Ex As Exception
			Console.Error.WriteLine(ex)
		End Try
	End Sub
	
	' ---------------------------------------------------------------------------------
	' randomly from a normal distribution with mean 0 and variance 1
	' references:
	'  http://docs.oracle.com/javase/jp/8/docs/api/java/util/Random.html#nextGaussian--
	'  https://en.wikipedia.org/wiki/Marsaglia_polar_method
	Dim nextNextGaussian As Double = 0.0
	Dim haveNextNextGaussian As Boolean = False
	Function NextGaussian(rand As Random) As Double
		If haveNextNextGaussian Then
			haveNextNextGaussian = False
			NextGaussian = nextNextGaussian
			Exit Function
		End If
		Dim v1 As Double, v2 As Double, s As Double
		Do
			v1 = 2.0 * rand.nextDouble() - 1.0
			v2 = 2.0 * rand.nextDouble() - 1.0
			s = v1 * v1 + v2 * v2
		Loop While s >= 1.0 OrElse s = 0.0
		Dim multiplier As Double = Math.Sqrt(-2.0 * Math.Log(s) / s)
		nextNextGaussian = v2 * multiplier
		haveNextNextGaussian = True
		NextGaussian = v1 * multiplier
	End Function

End Module