TopCoder Marathon Match 26 (Practice) My Solution
=================================================

Problem Statement   
https://community.topcoder.com/longcontest/?module=ViewProblemStatement&rd=11148&compid=7535  


Standings  
https://community.topcoder.com/longcontest/?module=ViewStandings&rd=11148  


Submit  
https://community.topcoder.com/longcontest/?module=Submit&compid=7535&rd=11148&cd=10385  

Marathon Match List for Practice  
https://community.topcoder.com/longcontest/?module=ViewPractice  

