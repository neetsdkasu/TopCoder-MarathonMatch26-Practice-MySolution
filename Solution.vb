Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer	On

Imports System

Class XorShift
    Shared UMAX As Double = CDbl(ULong.MaxValue)
    Dim Value As ULong = 88172645463325252UL
    Public Function NextULong() As ULong
        Value = Value Xor (Value << 13)
        Value = Value Xor (Value >> 17)
        Value = Value Xor (Value << 5)
        NextULong = Value
    End Function
    Public Function NextULong(n As ULong) As ULong
        NextULong = NextULong() Mod n
    End Function
    Public Function NextULong(a As ULong, b As ULong) As ULong
        NextULong = NextULong() Mod (b - a) + a
    End Function
    Public Function NextDouble() As Double
        NextDouble = CDbl(NextULong()) / UMAX
    End Function
End Class


Public Class Permute
	
	Public Function findOrder(w() As Double) As Integer()
		Dim t0 As Integer = Environment.TickCount + 29700
		Dim t1 As Integer
		Dim XSFT As New XorShift()
		
		Dim N As Integer = CInt(Math.Floor(Math.Sqrt(CDbl(w.Length))))
		
		Dim ret(N - 1) As Integer
		Dim tmp(N - 1) As Integer
		For i As Integer = 0 To N - 1
			ret(i) = i
			tmp(i) = i
		Next i
		
		Dim score As Double = 0.0
		For i As Integer = 0 To N - 2
			For j As Integer = i + 1 To N - 1
				score += w(tmp(i) * N + tmp(j))
			Next j
		Next i
		Dim maxScore As Double = score
		Dim baseScore As Double = score
		
		Const Alpha As Double = 0.0005
		Const MaxTime As Double = 10000000.0
		
		Dim UN As ULong = CULng(N)
		Dim c As Integer = 0
		
		Do

			c += 1
			
			t1 = Environment.TickCount
			If t0 < t1 Then Exit Do
			
			Dim i As Integer = CInt(XSFT.NextULong(UN))
			Dim j As Integer = (i + 1 + CInt(XSFT.NextULong(UN - 1UL))) Mod N
            
			If j < i Then
				Dim k As Integer = i
				i = j
				j = k
			End If
			
			Dim tmpi As Integer = tmp(i)
			Dim tmpj As Integer = tmp(j)
			Dim tmpiN As Integer = tmpi * N
			Dim tmpjN As Integer = tmpj * N
            
			Dim tmpScore As Double = baseScore + w(tmpjN + tmpi) - w(tmpiN + tmpj)
			For k As Integer = i + 1 To j - 1
				Dim tmpk As Integer = tmp(k)
				Dim tmpkN As Integer = tmpk * N
				tmpScore += w(tmpjN + tmpk) + w(tmpkN + tmpi) - w(tmpiN + tmpk) - w(tmpkN + tmpj)
			Next k

			If tmpScore > maxScore Then
				tmp(i) = tmpj
				tmp(j) = tmpi
				maxScore = tmpScore
				baseScore = tmpScore
				Array.Copy(tmp, ret, ret.Length)
				Continue Do
			End If
			If tmpScore > baseScore Then
				tmp(i) = tmpj
				tmp(j) = tmpi
				baseScore = tmpScore
				Continue Do
			End If
			Dim te As Double = Math.Pow(Alpha, CDbl(c) / MaxTime)
			Dim ee As Double = Math.Exp(CDbl(tmpScore - baseScore) / te)
			
			Dim rand As Double = XSFT.NextDouble()
			If rand < ee Then
				tmp(i) = tmpj
				tmp(j) = tmpi
				baseScore = tmpScore
				Continue Do
			End If
			
		Loop
        
		maxScore /= CDbl(N) * Math.Sqrt(CDbl(N) * Math.Log(CDbl(N)) - CDbl(N)) / 100.0
		
		Console.Error.WriteLine("Score:{0}", maxScore)
		Console.Error.WriteLine("cycles:{0}, rate:{1}", c, CDbl(c) / MaxTime)
		
		findOrder = ret
		
	End Function

End Class